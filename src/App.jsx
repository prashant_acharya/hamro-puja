import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

/* Components Import */
import Nav from './components/nav/Nav'
import Home from './components/home/Home'
import Packages from './components/packages/Packages'
import PujaSamagri from './components/pujasamagri/Pujasamagri'
import BrahminSewa from './components/brahminsewa/Brahminsewa'
import JyotishSewa from './components/jyotishsewa/JyotishSewa'
import Footer from './components/footer/Footer'

function App() {
	return (
		<div>
			<Router>
				<Nav />
				<Route exact path='/' component={Home} />
				<Route path='/packages' component={Packages} />
				<Route path='/pujasamagri' component={PujaSamagri} />
				<Route path='/brahminsewa' component={BrahminSewa} />
				<Route path='/jyotishsewa' component={JyotishSewa} />
				<Footer />
			</Router>
		</div>
	)
}

export default App
