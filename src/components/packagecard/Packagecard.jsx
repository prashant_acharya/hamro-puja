import React, { Component } from 'react'
import Modal from './Modal'

class PackageCard extends Component {
	state = {
		cartStatus: false
	}

	addToCart = () => {
		this.setState({
			cartStatus: !this.state.cartStatus
		})
	}

	getTotalPrice = () => {
		let total = 0
		this.props.package.items.forEach(item => {
			total += item.price
		})

		return total
	}

	render() {
		return (
			<div className="col-md-4 col-sm-1 col-lg-4">
				<div className="card">
					<img className="card-img-top" src="https://images.unsplash.com/photo-1517303650219-83c8b1788c4c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bd4c162d27ea317ff8c67255e955e3c8&auto=format&fit=crop&w=2691&q=80" alt="Card image cap" />
					<div className="card-body">
						<h4 className="card-title">{this.props.package.packageName}</h4>
						<p><span className="card-title">Price: </span> {this.getTotalPrice()} </p>
						<div class="btn-group text-center" role="group" aria-label="Basic example">
							<button type="button" className="btn btn-default" data-toggle="modal" data-target={`#${this.props.package.id}`}>
								View More
							</button>
							<Modal
								package={this.props.package}
								addToCart={this.addToCart}
								cartStatus={this.state.cartStatus}
							/>
							{
								this.state.cartStatus ? (
									<button
										className="btn btn-danger"
										onClick={this.addToCart}>
										- Remove from Cart
										</button>
								) : (
									<button
										className="btn btn-default"
										onClick={this.addToCart}>
										+ Add to Cart
									</button>
								)
							}
						</div>
					</div>
				</div>
			</div>
		)
	}
}

export default PackageCard;