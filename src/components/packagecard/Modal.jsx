import React, { Component } from 'react'

class Modal extends Component {
	state = {}
	render() {
		return (
			<div className="modal fade" id={this.props.package.id} tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
				<div className="modal-dialog" role="document">
					<div className="modal-content">
						<div className="modal-header">
							<h5 className="modal-title" id="exampleModalLongTitle">{this.props.package.packageName}</h5>
							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<ul class="list-group">
								{
									this.props.package.items.map(item => (
										<li class="list-group-item">{item.name} - Rs {item.price}</li>
									))
								}
							</ul>
						</div>
						<div className="modal-footer">
							{
								this.props.cartStatus ? (
									<button
										className="btn btn-primary"
										onClick={this.props.addToCart}>
										- Remove from Cart
									</button>
									) : (
										<button
											className="btn btn-primary"
											onClick={this.props.addToCart}>
											+ Add to Cart
									</button>
								)
							}

						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Modal;