export const packages = [
	{
		id: 1,
		packageName: 'Item 1',
		items: [
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 50},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 200},
			{name: 'item-1', price: 1500}
		]
	},
	{
		id: 2,
		packageName: 'Item 2',
		items: [
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 10},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 300},
			{name: 'item-1', price: 500}
		]
	},
	{
		id: 3,
		packageName: 'Item 3',
		items: [
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500}
		]
	},
	{
		id: 4,
		packageName: 'Item 4',
		items: [
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 100},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 300}
		]
	},
	{
		id: 5,
		packageName: 'Item 5',
		items: [
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500}
		]
	},
	{
		id: 6,
		packageName: 'Item 6',
		items: [
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500},
			{name: 'item-1', price: 500}
		]
	},
]