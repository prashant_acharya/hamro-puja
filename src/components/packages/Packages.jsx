import React, { Component } from 'react'
import PackageCard from '../packagecard/Packagecard'

import { packages } from './packageList'

class Packages extends Component {
    state = {
        packages: []
    }

    componentDidMount() {
        this.setState({ packages: packages })
    }

    render() {
        return (
            <div className='container' style={{'position': 'relative'}}>
                <div className="row" style={{'position': 'absolute', 'right': '50px'}}>
                    <div>
                        Cart: 0
                    </div>
                </div>
                <div className="row" style={{marginTop: '30px'}}>
                    {
                        this.state.packages.map(item => {
                            return <PackageCard package={item} key={item.id} />
                        })
                    }
                </div>
            </div>
        )
    }
}

export default Packages