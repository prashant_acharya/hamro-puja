import React, { Component } from 'react'
import { NavLink, Link } from 'react-router-dom'
import './nav.css'

class Nav extends Component {
	state = {}
	render() {
		return (
			<nav className="navbar navbar-expand-lg bg-white">
				<div className="container">
					<Link className="navbar-brand" to='/'>Hamro Puja</Link>
					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
						<span className="sr-only">Toggle navigation</span>
						<span className="navbar-toggler-icon"></span>
						<span className="navbar-toggler-icon"></span>
						<span className="navbar-toggler-icon"></span>
					</button>
					<div className="collapse navbar-collapse" id="navbarText">
						<ul className="navbar-nav ml-auto">
							<li className="nav-item active">
								<NavLink className="nav-link" to='/'>Home <span className="sr-only">(current)</span></NavLink>
							</li>
							<li className="nav-item">
								<NavLink className="nav-link" to='/packages'>Packages</NavLink>
							</li>
							<li className="nav-item">
								<NavLink className="nav-link" to='/pujasamagri'>Puja Samagri</NavLink>
							</li>
							<li className='nav-item'>
								<NavLink className='nav-link' to='/brahminsewa'>Brahmin Sewa</NavLink>
							</li>
							<li className='nav-item'>
								<NavLink className='nav-link' to='/jyotishsewa'>Jyotish Sewa</NavLink>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		);
	}
}

export default Nav