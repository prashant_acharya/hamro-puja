import React, { Component } from 'react'
import './footer.css'

class Footer extends Component {
	state = {}
	render() {
		return (
			<div className="container">
				<footer>
					<div>All rights reserved to Intercube Solutions</div>
				</footer>
			</div>
		)
	}
}

export default Footer