import React, { Component } from 'react'
import Carousel from './Carousel'
import Services from './Services'
import Features from './Features'
import './home.css'

class Home extends Component {
	state = {}
	render() {
		return (
			<div className="container">
				<Carousel />
				<Services />
				<Features />
			</div>
		)
	}
}

export default Home;