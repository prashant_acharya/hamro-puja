import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Services extends Component {
	state = {}
	render() {
		return (
			<div className='services text-center'>
				<h2 className='title'>Services</h2>
				<div className="row">
					<div className="col-md-4">
						<div className="card">
							<img className="card-img-top" src="https://images.unsplash.com/photo-1517303650219-83c8b1788c4c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bd4c162d27ea317ff8c67255e955e3c8&auto=format&fit=crop&w=2691&q=80" alt="Service" />
							<div className="card-body">
								<Link to='packages'><h4 className='title'>Puja Packages</h4></Link>
								<p>All materials required for some specific puja.</p>
							</div>
						</div>
					</div>
					<div className="col-md-4">
						<div className="card">
							<img className="card-img-top" src="https://images.unsplash.com/photo-1517303650219-83c8b1788c4c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bd4c162d27ea317ff8c67255e955e3c8&auto=format&fit=crop&w=2691&q=80" alt="Quality" />
							<div className="card-body">
								<Link to='brahminsewa'><h4 className='title'>Brahmin Sewa</h4></Link>
								<p>Book a Brahmin for your puja.</p>
							</div>
						</div>
					</div>
					<div className="col-md-4">
						<div className="card">
							<img className="card-img-top" src="https://images.unsplash.com/photo-1517303650219-83c8b1788c4c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=bd4c162d27ea317ff8c67255e955e3c8&auto=format&fit=crop&w=2691&q=80" alt="Delivery" />
							<div className="card-body">
								<Link to='jyotishsewa'><h4 className='title'>Jyotish Sewa</h4></Link>
								<p>Online jyotish sewa</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Services;