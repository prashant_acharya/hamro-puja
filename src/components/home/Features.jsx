import React, { Component } from 'react'
import delivery from './images/delivery.png'
import quality from './images/quality.png'


class Features extends Component {
	state = {}
	render() {
		return (
			<div className='services text-center'>
				<h2 className='title'>Features</h2>
				<div className="row">
					<div className="col-md-4">
						<div className="feature-icon">
							<img src={delivery} alt="" />
						</div>
						<div className="feature">
							<h4 className='title'>Home Delivery</h4>
						</div>
					</div>
					<div className="col-md-4">
						<div className="feature-icon">
							<img src={quality} alt="" />
						</div>
						<div className="feature">
							<h4 className='title'>Outstanding Quality and Customer Service</h4>
						</div>
					</div>
					<div className="col-md-4">
						<div className="feature-icon">
							<img src={delivery} alt="" />
						</div>
						<div className="feature">
							<h4 className='title'>Free Delivery</h4>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Features;