import React, { Component } from 'react'
import {Link} from 'react-router-dom'

class ProductModal extends Component {

	render() {
		const {...data} = this.props.data
		const imgSrc = `https://techtrix2019.herokuapp.com${data.imagePath}`
		const id = data['_id']
		return (
			<div>
				<a htef='#' style={{'cursor': 'pointer'}} className='nav-link' data-toggle='modal' data-target={`#${id}`}>View More</a>

				<div class="modal fade" id={`${id}`} tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h3 class="modal-title" id="exampleModalLabel">{data.title}</h3>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<div>
									<img src={imgSrc} className="img-fluid" alt=""/>
								</div> <br/>
								<strong>Price Per Day:</strong> {data.pricePerDay}<br/>
								<strong>Security Deposit:</strong> {data.collateralValue} <br/>
								<strong>Details: </strong> {data.details} <br/>
								<strong>Location: </strong> {data.location}
							</div>
							<div class="modal-footer">
								<Link className='btn btn-primary' to='/request'>Send a Request to Rent</Link>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
 
export default ProductModal;