import React, { Component } from 'react'

class Card extends Component {
	state = {}
	render() {
		return (
			<div className="col-3 product-wrapper">
				<div className="card">
					<div>
						<img className="card-img-top" style={{ 'maxHeight': '200px' }} src={imgSrc} alt="Card image cap" />
					</div>
					<div className="card-body">
						<h5 className='card-title'>{data.title}</h5>
						<p className="card-text">
							Rs <strong> {data.pricePerDay} </strong>/day <br />
							<i class="material-icons" style={{ 'fontSize': '12px' }}>location_on</i><strong> {data.location} </strong>
							<hr />
						</p>
						<ProductModal className='text-center' data={data} />
					</div>
				</div>
			</div>
		)
	}
}

export default Card