import React, { createContext } from 'react';
import ReactDOM from 'react-dom';
import App from './App';

export const Cart = createContext()

export class CartProvider extends React.Component {
	state = {
		hello: "world"
	}

	render() {
		return (
			<Cart.Provider value={this.state}>
				{this.props.children}
			</Cart.Provider>
		)
	}
}

ReactDOM.render(
	<CartProvider>
		<App />
	</CartProvider>
	, document.getElementById('root'));